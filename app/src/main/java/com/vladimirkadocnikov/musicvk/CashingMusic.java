package com.vladimirkadocnikov.musicvk;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * Created by vladimirkadocnikov on 23.08.16.
 */
public class CashingMusic {
    Context context;
    String url, fileName;
    File file = null;
    File[]  fileList;
    public CashingMusic(Context context,String url){
        this.context = context;
        this.url = url;
        fileName= Uri.parse(url).getLastPathSegment();
}
    public File saveMusic(){
        try {
            file =  File.createTempFile(fileName,null,context.getCacheDir());
          String path =  file.getPath();
            Log.d("DDD",path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
    public File[] openFile(){

        try {
            file =  File.createTempFile(null,null,context.getCacheDir());
            fileList = file.listFiles();
          int i =  fileList.length;
            Log.d("DDD",String.valueOf(i));
        } catch (IOException e) {
            e.printStackTrace();
        }

//file = new File("/data/user/0/com.vladimirkadocnikov.musicvk/cache/f1aab3c12e09.mp3-39840016.tmp").getAbsoluteFile();
return fileList;    }
    public boolean deleteFile(){
        context.deleteFile(fileName);
        return true;
    }
//    /data/user/0/com.vladimirkadocnikov.musicvk/cache/f1aab3c12e09.mp3-39840016.tmp
}
