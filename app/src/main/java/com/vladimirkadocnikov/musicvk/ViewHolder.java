package com.vladimirkadocnikov.musicvk;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by vladimirkadocnikov on 09.03.16.
 */
public class ViewHolder {
    public TextView textViewArtist,
            textViewTitle,
            textViewDuration,
            textViewGenre;
    public ImageButton buttonPlay;
}