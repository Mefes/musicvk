package com.vladimirkadocnikov.musicvk;

/**
 * Created by vladimirkadocnikov on 28.06.16.
 */
public class ListItem {

    String Artist, Title,Uri,Duration;
    int     GenreId;
    boolean isPlaying = false;


    public ListItem(String Artist, String Title, int Duration, int GenreId) {
        super();
        this.Artist = Artist;
        this.Title = Title;
//        this.Duration = Duration;
//        this.GenreId = GenreId;
    }
    public ListItem(String Artist, String Title,String Uri,int Duration) {
        super();
        this.Artist = Artist;
        this.Title = Title;
        this.Uri = Uri;
        this.Duration = secTOmin(Duration);

    }

    public ListItem(String Artist, String Title,String Uri) {
        super();
        this.Artist = Artist;
        this.Title = Title;
        this.Uri = Uri;
    }
    public String getArtist() {
        return Artist;
    }

    public String getTitle() {
        return Title;
    }

    public String getDuration() {
        return Duration;
    }

    public int getGenreId() {
        return GenreId;
    }

    public String getUri() {
        return Uri;
    }

    public boolean getIsPlaying() {
        return isPlaying;
    }


    public void setArtist(String Artist) {
        this.Artist = Artist;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public void setDuration(int Duration) {
        this.Duration = secTOmin(Duration);
    }

    public void setGenreId(int GenreId) {
        this.GenreId = GenreId;
    }

    public void setUri(String Uri) {
        this.Uri = Uri;
    }

    public void setIsPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    private String secTOmin(int sec){
        String min;
        if ((sec%60)<10){
            min=String.valueOf(sec/60)+":0"+sec%60;
        }else{
            min=String.valueOf(sec/60)+":"+sec%60;
        }
return  min;
    }
}
