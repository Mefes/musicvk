package com.vladimirkadocnikov.musicvk;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by vladimirkadocnikov on 28.06.16.
 */
public class MusicCustomAdapter extends BaseAdapter implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {
    private static final int LAYOUT_0 = 0;
    private static final int MAX_LAYOUT_COUNT = 1;

    private ArrayList<ListItem> mData = new ArrayList<ListItem>();
    private LayoutInflater mInflater;
    Context context = null;
    ViewHolder holder = null;
    int flag = -1;
    MediaPlayer mediaPlayer = null;

    public MusicCustomAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        mediaPlayer = new MediaPlayer();
    }

    public void AddAudioItem(final ListItem listItem) {
        mData.add(listItem);
//        mAudios.add(mData.size() - 1);
        notifyDataSetChanged();
    }

    public void clear(){
        mData.clear();
    }

    @Override
    public int getItemViewType(int position) {

//            if (mAudios.contains(position))
        return LAYOUT_0;
    }

    @Override
    public int getViewTypeCount() {
        return MAX_LAYOUT_COUNT;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListItem listItem = mData.get(position);
        int type = getItemViewType(position);
        if (convertView == null) {
            holder = new ViewHolder();
            switch (type) {
                case LAYOUT_0:
                    convertView = mInflater.inflate(R.layout.audiolist_item, null);
                    holder.textViewArtist = (TextView) convertView.findViewById(R.id.textViewArtist);
                    holder.textViewTitle = (TextView) convertView.findViewById(R.id.textViewTitle);
                    holder.buttonPlay = (ImageButton) convertView.findViewById(R.id.imageButton);
                    holder.textViewDuration = (TextView) convertView.findViewById(R.id.textViewDuration);
                    //holder.textViewGenre = (TextView) convertView.findViewById(R.id.textViewNews);
                    //
                    holder.textViewArtist.setText(listItem.getArtist());
                    holder.textViewTitle.setText(listItem.getTitle());
                    holder.buttonPlay.setOnClickListener(playClickListener);
                    holder.buttonPlay.setTag(position);
                    holder.textViewDuration.setText(listItem.getDuration());
//                    holder.textViewGenre.setText(listItem.getGenreId());
                    break;
            }
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        switch (type) {
            case LAYOUT_0:
                holder.textViewArtist.setText(listItem.getArtist());
                holder.textViewTitle.setText(listItem.getTitle());
                holder.buttonPlay.setOnClickListener(playClickListener);
                holder.buttonPlay.setTag(position);
                holder.textViewDuration.setText(listItem.getDuration());
//                holder.textViewGenre.setText(listItem.getGenreId());
                break;
        }
        if (listItem.getIsPlaying()){
            holder.buttonPlay.setImageResource(R.drawable.pause_button_selector);
        }
        else {
            holder.buttonPlay.setImageResource(R.drawable.play_button_selector);
        }
        convertView.setTag(holder);
        return convertView;
    }


    ListItem getListItem(int position) {
        return ((ListItem) getItem(position));
    }

    View.OnClickListener playClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();

//new CashingMusic(context,getListItem(pos).getUri()).saveMusic();

            notifyDataSetChanged();//refresh views

            try {
                if (mediaPlayer.isPlaying()) {

                    if (getListItem(pos).getIsPlaying()) {
                        mediaPlayer.pause();
                        flag = pos;
                        getListItem(pos).setIsPlaying(false);
                    } else {
                        mediaPlayer.pause();
                        mediaPlayer.reset();
                        mediaPlayer.setDataSource(context, Uri.parse(getListItem(pos).getUri()));
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        mediaPlayer.setOnPreparedListener(MusicCustomAdapter.this);
                        mediaPlayer.setOnCompletionListener(MusicCustomAdapter.this);
                        mediaPlayer.prepareAsync();
                        flag = -1;
                        Playing(pos);


                    }
                } else {
                    if (flag != -1) {
                        if (flag == pos) {
                            mediaPlayer.start();
                            Playing(pos);
                            flag = -1;
                        } else {
                            mediaPlayer.pause();
                            mediaPlayer.reset();
                            mediaPlayer.setDataSource(context, Uri.parse(getListItem(pos).getUri()));
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            mediaPlayer.setOnPreparedListener(MusicCustomAdapter.this);
                            mediaPlayer.setOnCompletionListener(MusicCustomAdapter.this);
                            mediaPlayer.prepareAsync();
                            flag = -1;
                            Playing(pos);
                        }
                    } else {
                        mediaPlayer.reset();
                        mediaPlayer.setDataSource(context, Uri.parse(getListItem(pos).getUri()));
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        mediaPlayer.setOnPreparedListener(MusicCustomAdapter.this);
                        mediaPlayer.setOnCompletionListener(MusicCustomAdapter.this);
                        mediaPlayer.prepareAsync();
                        flag = -1;
                        Playing(pos);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    protected void Playing(int position) {
        for (int i = 0; i < mData.size(); i++) {
            if (getListItem(i).getIsPlaying()) {
                getListItem(i).setIsPlaying(false);
            }
        }
        getListItem(position).setIsPlaying(true);

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        try {

            notifyDataSetChanged();//refresh views

            for (int i = 0; i < mData.size(); i++) {
                if (getListItem(i).getIsPlaying()) {
                    getListItem(i).setIsPlaying(false);
                    mediaPlayer.reset();
                    mediaPlayer.setDataSource(context, Uri.parse(getListItem(i + 1).getUri()));
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setOnPreparedListener(MusicCustomAdapter.this);
                    mediaPlayer.setOnCompletionListener(MusicCustomAdapter.this);
                    mediaPlayer.prepareAsync();
                    flag = -1;
                    Playing(i+1);
                    break;
                }
            }
        } catch (IOException e) {

        }

    }
}
