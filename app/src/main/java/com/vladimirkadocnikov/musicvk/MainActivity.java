package com.vladimirkadocnikov.musicvk;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiAudio;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.util.VKUtil;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String[] scope = new String[]{VKScope.AUDIO};
    ListView listView;
    TextView nameTextView,cityTextView;
    ImageView photoImageView;
    MusicCustomAdapter musicCustomAdapter = null;
    VKParameters params;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        musicCustomAdapter= new MusicCustomAdapter(this);
        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        for(int i = 0; i<fingerprints.length;i++)
            Log.i("myApp", "Fingerprint:"+fingerprints[i]);
        params = new VKParameters();
        params.put(VKApiConst.FIELDS,"photo_100,city");
//        params.put(VKApiConst.FIELDS,"city");
//        params.put(VKApiCost.OWNER_ID, "1");
//        params.put(VKApiConst.COUNT, "3");
        VKSdk.login(this, scope);
//        VKSdk.initialize(this);
//
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ViewGroup viewGroup = (ViewGroup) view;
//                musicCustomAdapter.getView(position,view,parent).;
//            }
//        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        musicCustomAdapter.clear();
        if (id == R.id.nav_camera) {
            // Handle the camera action
            vkRequestHandler(VKApi.audio().getRecommendations());
        } else if (id == R.id.nav_gallery) {
            vkRequestHandler(VKApi.audio().getPopular());
        } else if (id == R.id.nav_slideshow) {
            vkRequestHandler(VKApi.audio().get());
        } else if (id == R.id.nav_manage) {
//            File[] files = new CashingMusic(this,)
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {

            @Override
            public void onResult(VKAccessToken res) {
                // Пользователь успешно авторизовался
                VKRequest  request = VKApi.users().get(params);
                request.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        super.onComplete(response);
                        listView = (ListView) findViewById(R.id.audiolist);
                        nameTextView = (TextView )findViewById(R.id.nameTextView);
                        cityTextView = (TextView)findViewById(R.id.cityTextView);
                        photoImageView = (ImageView) findViewById( R.id.imageView);
//
                        VKList<VKApiUser> userVKList = (VKList<VKApiUser>) response.parsedModel;
                        nameTextView.setText(userVKList.get(0).first_name + " " +userVKList.get(0).last_name);
                        new ImageDownloader(photoImageView).execute(userVKList.get(0).photo_100);
                        if (userVKList.get(0).online){

                            cityTextView.setText("Не в сети");

                        }else {
                            cityTextView.setText("Онлайн");
                        }
                    }
                });

                vkRequestHandler(VKApi.audio().get());

            }

            @Override
            public void onError(VKError error) {
                // Произошла ошибка авторизации (например, пользователь запретил авторизацию)
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void vkRequestHandler(VKRequest vkRequest){
        vkRequest.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                VKList<VKApiAudio> audioVKList = (VKList<VKApiAudio>) response.parsedModel;
                for (int i = 0; i < audioVKList.size(); i++) {
                    musicCustomAdapter.AddAudioItem(new ListItem(audioVKList.get(i).artist,audioVKList.get(i).title,audioVKList.get(i).url,audioVKList.get(i).duration));
                }
                listView.setAdapter(musicCustomAdapter);

            }
        });

    }
}
